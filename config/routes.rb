Rails.application.routes.draw do

  namespace :api, defaults: {format: "json"} do
    namespace :v1 do
      post 'users' => 'user#create'
      post 'login' => 'user#login'
    end
  end

end
