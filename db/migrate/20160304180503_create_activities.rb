class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|

      t.string :name
      t.date :event_date
      t.string :start_hour
      t.text :description
      t.integer :user_id
      t.timestamps null: false
    end
  end
end
