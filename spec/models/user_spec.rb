require 'rails_helper'

RSpec.describe User, type: :model do
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password) }
  it { should_not allow_value("this is a BAD FORMAT mail").for(:email) }
  it { should allow_value("test@test.com  ").for(:email) }
end
