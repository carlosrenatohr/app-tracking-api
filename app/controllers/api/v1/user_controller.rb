class Api::V1::UserController < Api::V1::ApiController

  def create
    # if (params[:email].nil?)
    #   error_response!('Email is not valid')
    # end
    @user = User.new(validate_params)
    if (@user.save)
      render json: {message: 'User created successful', response: 'success'}
    else
      error_response!(@user.errors.full_messages)
    end
  end

  def login
    @user = User.authenticate(validate_params)
    if (@user && !@user.nil?)
      output = {}
      output[:response] = 'success'
      for param in [:email]
        if (!@user[param].nil?)
          output[param] = @user[param]
        end
      end
      render json: output
    else
      error_response!('Email or password failed.')
    end
  end

  private
  def validate_params
    params.require(:user).permit(:email, :password)
  end

end