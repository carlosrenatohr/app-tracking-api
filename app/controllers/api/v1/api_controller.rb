class Api::V1::ApiController < ApplicationController

  def error_response!(msg, status = 404)
    response.status = status
    render json:{
      response: 'error',
      message: msg
    }
  end
end