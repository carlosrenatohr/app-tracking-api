class User < ActiveRecord::Base
  validates :email, presence: true, email: true, uniqueness: true
  validates :password, presence: true
  # attr_accessor :password
  before_save :encrypt_password

  def encrypt_password
    if (self.password.nil?)
      return
    end
    self.password_salt = BCrypt::Engine.generate_salt(8)
    a = OpenSSL::Random.random_bytes(1).bytes[0] % 10
    b = OpenSSL::Random.random_bytes(1).bytes[0] % 10
    tmp_pass = a.to_s + b.to_s + self.password.to_s
    self.password = BCrypt::Engine.hash_secret(tmp_pass, password_salt)
  end

  def self.authenticate(data)
    user = User.find_by_email(data[:email])
    a = OpenSSL::Random.random_bytes(1).bytes[0] % 10
    b = OpenSSL::Random.random_bytes(1).bytes[0] % 10
    found = false

    for i in 0..9
      for j in 0..9
        d1 = ((i + a) % 10).to_s(10)
        d2 = ((j + b) % 10).to_s(10)
        tmp_password = d1 + d2 + data[:password]

        if user && user.password == BCrypt::Engine.hash_secret(tmp_password, user.password_salt)
          found = true
          break
        end
      end
      if found
        break
      end
    end

    if found
      user
    else
      nil
    end
  end

  def self.pass_bash
    a = OpenSSL::Random.random_bytes(1).bytes[0] % 10
    b = OpenSSL::Random.random_bytes(1).bytes[0] % 10

    password_salt = BCrypt::Engine.generate_salt(8)
    temporal = a.to_s(10) + b.to_s(10) + 'renato'

    final = BCrypt::Engine.hash_secret(temporal, password_salt)
    for i in 0..9
      for j in 0..9
        d1 = ((i + a) % 10).to_s(10)
        d2 = ((j + b) % 10).to_s(10)
        tmp_password = d1 + d2 + 'renato'

        if final == BCrypt::Engine.hash_secret(tmp_password, password_salt)
          found_it = true
          break
        end
      end
      if found_it
        break
      end
    end
    tmp_password + "///  d1: #{d1}///d2: #{d2} //salt: #{password_salt}// pass: #{final}"
  end

end
